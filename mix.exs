defmodule UeberauthInfiniteAisle.Mixfile do
  use Mix.Project

  @version "1.0.0"

  def project do
    [
      app: :ueberauth_infiniteaisle,
      version: @version,
      package: package(),
      elixir: "~> 1.3",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      source_url: "https://gitlab.com/infiniteaisle/open-source/ueberauth_infiniteaisle",
      homepage_url: "https://gitlab.com/infiniteaisle/open-source/ueberauth_infiniteaisle",
      description: description(),
      deps: deps(),
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :ueberauth, :oauth2]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:oauth2, "~> 0.9"},
      {:ueberauth, "~> 0.4"},

      # dev/test only dependencies
      {:credo, "~> 1.0", only: [:dev, :test]},
      {:exvcr, "~> 0.10.0", only: [:test]},

      # docs dependencies
      {:earmark, ">= 0.0.0", only: :dev},
      {:ex_doc, ">= 0.0.0", only: :dev}
    ]
  end

  defp docs do
    [extras: ["README.md"]]
  end

  defp description do
    "An Ueberauth strategy for using Infinite Aisle to authenticate your users."
  end

  defp package do
    [
      name: "ueberauth_infiniteaisle_strategy",
      files: ["lib", "mix.exs", "README*", "LICENSE*"],
      maintainers: ["Infinite Aisle"],
      licenses: ["MIT"],
      links: %{GitHub: "https://gitlab.com/infiniteaisle/open-source/ueberauth_infiniteaisle"}
    ]
  end
end
