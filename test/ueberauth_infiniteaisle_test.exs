defmodule UeberauthInfiniteAisleTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  doctest UeberauthInfiniteAisle

  @tag :skip
  test "handle_request!" do
    use_cassette "handle_request!" do
      conn = %Plug.Conn{
        params: %{
          client_id: "12345",
          client_secret: "98765",
          redirect_uri: "http://localhost:4000/auth/infiniteaisle/callback",
        }
      }
      result = Ueberauth.Strategy.InfiniteAisle.handle_request!(conn)
      assert result == nil
    end
  end

  describe "handle_callback!" do
    test "with no code"  do
      conn = %Plug.Conn{}
      result = Ueberauth.Strategy.InfiniteAisle.handle_callback!(conn)
      failure = result.assigns.ueberauth_failure
      assert length(failure.errors) == 1
      [no_code_error] = failure.errors

      assert no_code_error.message_key == "missing_code"
      assert no_code_error.message == "No code received"
    end
  end

  describe "handle_cleanup!" do
    test "clears infiniteaisle_user from conn" do
      conn = %Plug.Conn{}
       |> Plug.Conn.put_private(:infiniteaisle_user, %{username: "mtchavez"})

      result = Ueberauth.Strategy.InfiniteAisle.handle_cleanup!(conn)
      assert result.private.infiniteaisle_user == nil
    end
  end

  describe "uid" do
    test "uid_field not found" do
      conn = %Plug.Conn{}
       |> Plug.Conn.put_private(:infiniteaisle_user, %{uid: "not-found-uid"})

      assert Ueberauth.Strategy.InfiniteAisle.uid(conn) == nil
    end

    test "uid_field returned" do
      uid = "abcd1234abcd1234"
      conn = %Plug.Conn{}
       |> Plug.Conn.put_private(:infiniteaisle_user, %{"email" => uid})

      assert Ueberauth.Strategy.InfiniteAisle.uid(conn) == uid
    end
  end

  describe "credentials" do
    test "are returned" do
      conn = %Plug.Conn{}
       |> Plug.Conn.put_private(:infiniteaisle_token, %{access_token: "access-token", refresh_token: "refresh-token", expires: false, expires_at: Time.utc_now(), token_type: "access_code", other_params: %{}})
       creds = Ueberauth.Strategy.InfiniteAisle.credentials(conn)
       assert creds.token == "access-token"
       assert creds.refresh_token == "refresh-token"
       assert creds.expires == true
       assert creds.scopes == [""]
    end
  end

  describe "info" do
    test "is returned" do
      conn = %Plug.Conn{}
       |> Plug.Conn.put_private(:infiniteaisle_user, %{
         "name" => "mtchavez",
         "username" => "mtchavez",
         "email" => "m@t.chavez",
         "location" => "",
         "avatar_url" => "http://the.image.url",
         "web_url" => "https://gitlab.com/mtchavez",
         "website_url" => "",
       })

      info = Ueberauth.Strategy.InfiniteAisle.info(conn)
      assert info.name == "mtchavez"
      assert info.email == "m@t.chavez"
      assert info.image == "http://the.image.url"
    end
  end
end
