defmodule Ueberauth.Strategy.InfiniteAisle do
  @moduledoc """
  Provides an Ueberauth strategy for authenticating with Infinite Aisle.

  ### Setup

  Create an application in Infinite Aisle::Identity for you to use.

  Register a new application at: [your gitlab developer page](https://gitlab.com/settings/developers) and get the `client_id` and `client_secret`.

  Include the provider in your configuration for Ueberauth

      config :ueberauth, Ueberauth,
        providers: [
          infiniteaisle: { Ueberauth.Strategy.InfiniteAisle, [] }
        ]

  Then include the configuration for infinite aisle.

      config :ueberauth, Ueberauth.Strategy.InfiniteAisle.OAuth,
        client_id: System.get_env("INFINITEAISLE_CLIENT_ID"),
        client_secret: System.get_env("INFINITEAISLE_CLIENT_SECRET")

  If you haven't already, create a pipeline and setup routes for your callback handler

      pipeline :auth do
        Ueberauth.plug "/auth"
      end

      scope "/auth" do
        pipe_through [:browser, :auth]

        get "/:provider/callback", AuthController, :callback
      end


  Create an endpoint for the callback where you will handle the `Ueberauth.Auth` struct

      defmodule MyApp.AuthController do
        use MyApp.Web, :controller

        def callback_phase(%{ assigns: %{ ueberauth_failure: fails } } = conn, _params) do
          # do things with the failure
        end

        def callback_phase(%{ assigns: %{ ueberauth_auth: auth } } = conn, params) do
          # do things with the auth
        end
      end

  You can edit the behaviour of the Strategy by including some options when you register your provider.

  To set the `uid_field`

      config :ueberauth, Ueberauth,
        providers: [
          infiniteaisle: { Ueberauth.Strategy.InfiniteAisle, [uid_field: :email] }
        ]

  Default is `:email`

  To set the default 'scopes' (permissions):

      config :ueberauth, Ueberauth,
        providers: [
          infiniteaisle: { Ueberauth.Strategy.InfiniteAisle, [default_scope: "public"] }
        ]

  Default is "public"
  """
  use Ueberauth.Strategy,
    uid_field: :email,
    default_scope: "public",
    oauth2_module: Ueberauth.Strategy.InfiniteAisle.OAuth

  alias Ueberauth.Auth.Info
  alias Ueberauth.Auth.Credentials
  alias Ueberauth.Auth.Extra

  @doc """
  Handles the initial redirect to the infinite aisle authentication page.

  To customize the scope (permissions) that are requested by infinite aisle include them as part of your url:

      "/auth/infiniteaisle?scope=api read_user read_registry"

  You can also include a `state` param that infinite aisle will return to you.
  """
  def handle_request!(conn) do
    scopes = conn.params["scope"] || option(conn, :default_scope)
    send_redirect_uri = Keyword.get(options(conn) || [], :send_redirect_uri, true)

    opts =
      if send_redirect_uri do
        [redirect_uri: callback_url(conn), scope: scopes]
      else
        [scope: scopes]
      end

    opts =
      if conn.params["state"], do: Keyword.put(opts, :state, conn.params["state"]), else: opts

    module = option(conn, :oauth2_module)
    redirect!(conn, apply(module, :authorize_url!, [opts]))
  end

  @doc """
  Handles the callback from Infinite Aisle. When there is a failure from Infinite Aisle the failure is included in the
  `ueberauth_failure` struct. Otherwise the information returned from Infinite Aisle is returned in the `Ueberauth.Auth` struct.
  """
  def handle_callback!(%Plug.Conn{params: %{"code" => code}} = conn) do
    module = option(conn, :oauth2_module)
    token = apply(module, :get_token!, [[code: code]])

    if token.access_token == nil do
      set_errors!(conn, [
        error(token.other_params["error"], token.other_params["error_description"])
      ])
    else
      fetch_user(conn, token)
    end
  end

  @doc false
  def handle_callback!(conn) do
    set_errors!(conn, [error("missing_code", "No code received")])
  end

  @doc """
  Cleans up the private area of the connection used for passing the raw Infinite Aisle response around during the callback.
  """
  def handle_cleanup!(conn) do
    conn
    |> put_private(:infiniteaisle_user, nil)
  end

  @doc """
  Fetches the uid field from the Infinite Aisle response. This defaults to the option `uid_field` which in-turn defaults to `email`
  """
  def uid(conn) do
    user =
      conn
      |> option(:uid_field)
      |> to_string

    conn.private.infiniteaisle_user[user]
  end

  @doc """
  Includes the credentials from the Infinite Aisle response.
  """
  def credentials(conn) do
    token = conn.private.infiniteaisle_token
    scope_string = token.other_params["scope"] || ""
    scopes = String.split(scope_string, ",")

    %Credentials{
      token: token.access_token,
      refresh_token: token.refresh_token,
      expires_at: token.expires_at,
      token_type: token.token_type,
      expires: !!token.expires_at,
      scopes: scopes
    }
  end

  @doc """
  Fetches the fields to populate the info section of the `Ueberauth.Auth` struct.
  """
  def info(conn) do
    user = conn.private.infiniteaisle_user

    %Info{
      name: user["name"],
      email: user["email"],
      image: user["avatar_url"],
    }
  end

  @doc """
  Stores the raw information (including the token) obtained from the Infinite Aisle callback.
  """
  def extra(conn) do
    %Extra{
      raw_info: %{
        token: conn.private.infiniteaisle_token,
        user: conn.private.infiniteaisle_user
      }
    }
  end

  defp fetch_user(conn, token) do
    conn = put_private(conn, :infiniteaisle_token, token)
    api_ver = option(conn, :api_ver) || "v1"

    case Ueberauth.Strategy.InfiniteAisle.OAuth.get(token, "/api/#{api_ver}/user") do
      {:ok, %OAuth2.Response{status_code: 401, body: _body}} ->
        set_errors!(conn, [error("token", "unauthorized")])

      {:ok, %OAuth2.Response{status_code: status_code, body: user}}
      when status_code in 200..399 ->
        put_private(conn, :infiniteaisle_user, user)

      {:error, %OAuth2.Error{reason: reason}} ->
        set_errors!(conn, [error("OAuth2", reason)])
    end
  end

  defp option(conn, key) do
    Keyword.get(options(conn) || [], key, Keyword.get(default_options(), key))
  end
end
