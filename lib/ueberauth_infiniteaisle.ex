defmodule UeberauthInfiniteAisle do
  @moduledoc false

  alias Ueberauth.Strategy.InfiniteAisle.OAuth

  def sign_out(access_token, _refresh_token) do
    sign_out_url = :ueberauth
    |> Application.fetch_env!(Ueberauth.Strategy.InfiniteAisle.OAuth)
    |> Keyword.get(:sign_out_url, "https://identity.infiniteaisle.com/clients/sign_out")

    OAuth.delete(access_token, sign_out_url)
  end
end
