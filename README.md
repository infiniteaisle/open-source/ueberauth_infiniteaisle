# Überauth Infinite Aisle

[![pipeline status](https://gitlab.com/infiniteaisle/open-source/ueberauth_infiniteaisle/badges/master/pipeline.svg)](https://gitlab.com/infiniteaisle/open-source/ueberauth_infiniteaisle/commits/master)

> Infinite Aisle OAuth2 strategy for Überauth.

## Installation

1. Setup your application in our [identity][identity] service.

1. Add `:ueberauth_infiniteaisle` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:ueberauth_infiniteaisle, git: "https://gitlab.com/infiniteaisle/open-source/ueberauth_infiniteaisle.git", tag: "1.0"}]
    end
    ```

1. Add Infinite Aisle to your Überauth configuration:

    ```elixir
    config :ueberauth, Ueberauth,
      providers: [
        infiniteaisle: {Ueberauth.Strategy.InfiniteAisle, []}
      ]
    ```

1.  Update your provider configuration:

    ```elixir
    config :ueberauth, Ueberauth.Strategy.InfiniteAisle.OAuth,
      client_id: System.get_env("INFINITEAISLE_CLIENT_ID"),
      client_secret: System.get_env("INFINITEAISLE_CLIENT_SECRET"),
      redirect_uri: System.get_env("INFINITEAISLE_REDIRECT_URI")
    ```

1.  Include the Überauth plug in your controller:

    ```elixir
    defmodule MyApp.AuthController do
      use MyApp.Web, :controller

      pipeline :browser do
        plug Ueberauth
        ...
       end
    end
    ```

1.  Create the request and callback routes if you haven't already:

    ```elixir
    scope "/auth", MyApp do
      pipe_through :browser

      get "/:provider", AuthController, :request
      get "/:provider/callback", AuthController, :callback
    end
    ```

1. You controller needs to implement callbacks to deal with `Ueberauth.Auth` and `Ueberauth.Failure` responses.

For an example implementation see the [Überauth Example][example-app] application
on how to integrate other strategies. Adding InfiniteAisle should be similar to Gitlab.

[identity]: https://gitlab.com/infiniteaisle/services/identity
[example-app]: https://github.com/ueberauth/ueberauth_example

## Calling

Depending on the configured url you can initiate the request through:

    /auth/infiniteaisle

Or with options:

    /auth/infiniteaisle?scope=api read_user


```elixir
config :ueberauth, Ueberauth,
  providers: [
    identity: { Ueberauth.Strategy.Identity, [
        callback_methods: ["POST"],
        uid_field: :email,
        nickname_field: :username,
      ] },
    infiniteaisle: {Ueberauth.Strategy.InfiniteAisle, [default_scope: "read_user"]},
  ]
```
